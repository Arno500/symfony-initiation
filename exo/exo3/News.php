<?php

require "AbstractEntity.php";
require "TimestampableTrait.php";
require "Entity.php";

final class News extends AbstractEntity implements EntityInterface
{
    use TimestampableTrait;
}