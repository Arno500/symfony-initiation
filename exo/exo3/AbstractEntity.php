<?php

abstract class AbstractEntity
{
    protected $id;
    public function getId(): int {
        return $this->id;
    }
}