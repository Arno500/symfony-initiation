<?php


trait TimestampableTrait
{
    protected $createdAt;
    protected $updatedAt;

    public function setCreatedAt($createdAt): void {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt() : DateTime
    {
        return $this->createdAt;
    }

    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt() : DateTime
    {
        return $this->updatedAt;
    }
}