<?php


interface EntityInterface
{
    public function getId(): int;
}