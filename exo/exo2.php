<?php

class Singleton
{
    private static $instance;

    public function __construct($arg, $constructFlag = true)
    {
        if (!isset(static::$instance)) {
            static::$instance = new Singleton($arg, true);
        }
        if ($constructFlag) {
            // On fait des trucs, et on a même $arg
        }
        return static::$instance;
    }
}