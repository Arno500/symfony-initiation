<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Entities\EntityInterface;
use App\Entity\Entities\EntityTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity #(repositoryClass="App\Repository\ProductRepository")
 * @Gedmo\SoftDeleteable(timeAware=true)
 */
class Gif implements EntityInterface
{
    use EntityTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Album", inversedBy="gifCollection")
     */
    private $album;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="gifCollection", cascade={"persist"})
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="favorites")
     */
    private $favedBy;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->favedBy = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Gif
     */
    public function setUrl($url): self
    {
        $this->url = $url;
        return $this;
    }


    /**
     * @param Album|null $album
     * @return Gif
     */
    public function setAlbum(Album $album): self
    {
        $this->album = $album;
        return $this;
    }

    /**
     * @return Album|null
     */
    public function getAlbum(): ?Album
    {
        return $this->album;
    }

    /**
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection $tags
     * @return self
     */
    public function setTags(ArrayCollection $tags): ?self
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isFavedBy(User $user): bool
    {
        return $this->favedBy->contains($user);
    }

    /**
     * @param User $user
     * @return self
     */
    public function switchFav(User $user): self
    {
        if ($this->isFavedBy($user)) {
            $this->favedBy->removeElement($user);
        } else {
            $this->favedBy->add($user);
        }
        return $this;
    }


}