<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Entities\EntityInterface;
use App\Entity\Entities\EntityTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity #(repositoryClass="App\Repository\ProductRepository")
 * @Gedmo\SoftDeleteable(timeAware=true)
 */
class Album implements EntityInterface
{
    use EntityTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Column(type="string", length=160)
     * @Assert\Length(
     *     max = 80,
     *     maxMessage = "Trop long !"
     *  )
     */
    private $title;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="albumCollection")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @var News[]
     * @ORM\OneToMany(targetEntity="App\Entity\Gif", mappedBy="album")
     * @ORM\JoinColumn(nullable=false)
     */
    private $gifCollection;

    public function __construct()
    {
        $this->gifCollection = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Album
     */
    public function setTitle($title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param User|null $author
     * @return Album
     */
    public function setAuthor(User $author): self
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @return Collection|null
     */
    public function getGifs(): Collection
    {
        return $this->gifCollection;
    }

}