<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Entities\EntityInterface;
use App\Entity\Entities\EntityTrait;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity #(repositoryClass="App\Repository\ProductRepository")
 * @Gedmo\SoftDeleteable(timeAware=true)
 */
class News implements EntityInterface
{
    use EntityTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *     min = 3,
     *     max = 100000,
     *     minMessage = "Il faut plus de {{ limit }} caractères",
     *     maxMessage = "Trop long"
     *  )
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *     min = 3,
     *     max = 100000,
     *     minMessage = "Il faut plus de {{ limit }} caractères",
     *     maxMessage = "Trop long"
     *  )
     */
    private $content;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="newsCollection")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return News
     */
    public function setTitle($title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return News
     */
    public function setContent($content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @param User|null $author
     * @return News
     */
    public function setAuthor(User $author): self
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

}