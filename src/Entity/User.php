<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{

    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var News[]
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="author")
     */
    private $newsCollection;

    /**
     * @var News[]
     * @ORM\OneToMany(targetEntity="App\Entity\Album", mappedBy="author")
     */
    private $albumCollection;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Gif", inversedBy="favedBy")
     * @ORM\JoinTable(name="user_favs")
     */
    private $favorites;

    public function __construct()
    {
        $this->newsCollection = new ArrayCollection();
        $this->albumCollection = new ArrayCollection();
        $this->favorites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        // $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles = [self::ROLE_USER, self::ROLE_ADMIN];

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @param News $news
     * @return self
     */
    public function addNews(News $news): self
    {
        $this->newsCollection->add($news);

        return $this;
    }

    /**
     * @param News $news
     * @return self
     */
    public function removeNews(News $news): self
    {
        $this->newsCollection->remove($news);

        return $this;
    }

    /**
     * @param Gif $gif
     * @return bool
     */
    public function faving(Gif $gif): bool
    {
        return $this->favorites->contains($gif);
    }

    /**
     * @param Gif $gif
     * @return self
     */
    public function switchFav(Gif $gif): self
    {
        if ($this->faving($gif)) {
            $this->favorites->removeElement($gif);
        } else {
            $this->favorites->add($gif);
        }
        return $this;
    }
}
