<?php

namespace App\Entity\Entities;

interface EntityInterface
{
    public function getId(): int;
}