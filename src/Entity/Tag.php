<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Entities\EntityInterface;
use App\Entity\Entities\EntityTrait;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity #(repositoryClass="App\Repository\ProductRepository")
 * @Gedmo\SoftDeleteable(timeAware=true)
 */
class Tag implements EntityInterface
{
    use EntityTrait;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Gif", mappedBy="tags")
     * @JoinTable(name="gifs_tags")
     */
    private $gifCollection;

    public function __construct()
    {
        $this->gifCollection = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return self
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getGifs()
    {
        return $this->gifCollection;
    }

    public function __toString(): string
    {
        return $this->name;
    }

}