<?php


namespace App\Controller;


use App\Entity\News;
use App\Form\NewsForm;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/news", name="app_news_index")
     * @param NewsRepository $newsRepository
     * @return Response
     */
    public function index(NewsRepository $newsRepository)
    {
        $news = $newsRepository->findBy([], ["id" => "DESC"], 10);

        return $this->render('news/index.html.twig', [
            'news' => $news,
        ]);

    }

    /**
     * @Route("/news/create", name="app_news_create")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(NewsForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news = $form->getData();
            $news->setAuthor($this->getUser());
            $entityManager->persist($news);
            $entityManager->flush();
            return $this->redirectToRoute("app_news_index");
        }

        return $this->render('news/add.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/news/{news}/update", name="app_news_update")
     * @param News $news
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function update(News $news, Request $request, EntityManagerInterface $entityManager)
    {
        if ($news->getAuthor() !== $this->getUser()) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
        $form = $this->createForm(NewsForm::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute("app_news_index");
        }

        return $this->render('news/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/news/{news}/remove", name="app_news_remove")
     * @param News $news
     * @param EntityManagerInterface $entityManager
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function remove(News $news, EntityManagerInterface $entityManager)
    {
        if ($news->getAuthor() !== $this->getUser()) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
        $entityManager->remove($news);
        $entityManager->flush();
        return new RedirectResponse($this->generateUrl("app_news_index"));
    }

}