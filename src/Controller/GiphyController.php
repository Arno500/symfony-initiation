<?php


namespace App\Controller;


use App\Entity\Album;
use App\Entity\Gif;
use App\Entity\Tag;
use App\Form\AlbumForm;
use App\Form\GifForm;
use App\Repository\AlbumRepository;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GiphyController extends AbstractController
{

    /**
     * @Route("/gifs/", name="app_gifs_album_index")
     * @param AlbumRepository $albumRepository
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function indexAlbum(AlbumRepository $albumRepository, TagRepository $tagRepository)
    {

        $albums = $albumRepository->findBy([], ["id" => "DESC"]);
        $tags = $tagRepository->findBy([], ["id" => "DESC"]);

        return $this->render('album/index.html.twig', [
            'albums' => $albums,
            'tags' => $tags
        ]);

    }

    /**
     * @Route("/gifs/create", name="app_gifs_album_create")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function addAlbum(Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(AlbumForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $album = $form->getData();
            $album->setAuthor($this->getUser());
            $entityManager->persist($album);
            $entityManager->flush();
            return $this->redirectToRoute("app_gifs_album_view", ["album" => $album->getId()]);
        }

        return $this->render('album/add.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/gifs/{album}/edit", name="app_gifs_album_edit")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param Album $album
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function editAlbum(Request $request, EntityManagerInterface $entityManager, Album $album)
    {
        if ($album->getAuthor() !== $this->getUser()) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
        $form = $this->createForm(AlbumForm::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute("app_gifs_album_view", ["album" => $album->getId()]);
        }

        return $this->render('album/edit.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/gifs/{album}", name="app_gifs_album_view")
     * @param Album $album
     * @return Response
     */
    public function showAlbum(Album $album)
    {
        return $this->render('album/show.html.twig', [
            'album' => $album,
        ]);
    }

    /**
     * @Route("/gifs/{album}/favs", name="app_gifs_favs")
     * @param Album $album
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function showFavs(Album $album)
    {
        return $this->render('album/favs.html.twig', [
            'album' => $album
        ]);
    }

    /**
     * @Route("/gifs/{album}/add", name="app_gifs_create")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param Album $album
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function add(Request $request, EntityManagerInterface $entityManager, Album $album)
    {
        $form = $this->createForm(GifForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gif = $form->getData();
            $gif->setAlbum($album);
            $entityManager->persist($gif);
            $entityManager->flush();
            return $this->redirectToRoute("app_gifs_album_view", ["album" => $album->getId()]);
        }

        return $this->render('gif/add.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/gifs/{album}/{gif}/fav", name="app_gifs_fav")
     * @param EntityManagerInterface $entityManager
     * @param Gif $gif
     * @param Album $album
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function fav(EntityManagerInterface $entityManager, Gif $gif, Album $album)
    {
        $user = $this->getUser();
        /** @noinspection PhpParamsInspection */
        $gif->switchFav($user);
        $user->switchFav($gif);
        $entityManager->flush();
        return $this->redirectToRoute("app_gifs_album_view", ["album" => $album->getId()]);
    }

    /**
     * @Route("/gifs/tag/{tag}", name="app_gifs_tag_search")
     * @ParamConverter("tag", options={"mapping": {"tag": "name"}})
     * @param Tag $tag
     * @return Response
     */
    public function showAByTag(Tag $tag)
    {
        return $this->render('tags/show.html.twig', [
            'tag' => $tag,
        ]);
    }

    /**
     * @Route("/gif/{album}/{gif}/delete", name="app_gifs_delete")
     * @param Gif $gif
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function remove(Gif $gif, EntityManagerInterface $entityManager)
    {
        $album = $gif->getAlbum();
        if ($album->getAuthor() !== $this->getUser()) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
        $entityManager->remove($gif);
        $entityManager->flush();
        return new RedirectResponse($this->generateUrl("app_gifs_album_view", ["album" => $album->getId()]));
    }
}