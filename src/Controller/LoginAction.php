<?php

namespace App\Controller;

use App\Form\LoginForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;

class LoginAction extends AbstractController
{

    private $templating;

    private $formFactory;

    public function __construct(Environment $templating, FormFactoryInterface $formFactory)
    {
        $this->templating = $templating;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/login", name="app_user_login")
     * @return Response
     */
    public function __invoke(AuthenticationUtils $authenticationUtils): Response
    {

        $form = $this->formFactory->create(LoginForm::class);
        $form->get('_username')->setData($authenticationUtils->getLastUsername());

        return new Response($this->templating->render("security/login.html.twig", [
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'form' => $form->createView()
        ]));
    }

}